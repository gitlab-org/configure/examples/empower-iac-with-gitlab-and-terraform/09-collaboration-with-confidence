terraform {
  required_providers {
    aws = {
      source  = "hashicorp/aws"
      version = "~> 4.52.0"
    }
  }

  backend "http" {}
}

provider "aws" {
  # Configured via environment variables
}

variable "environment" {
  type = string
}

variable "pet_name_length" {
  type = number
}

module "pet_site" {
  source  = "gitlab.com/gitlab-org/pet-site/aws"
  version = "~> 1.1"

  # Input Variables
  name            = "pet-collab"
  environment     = var.environment
  pet_name_length = var.pet_name_length
}

output "pet_name" {
  value = module.pet_site.pet_name
}

output "url" {
  value = module.pet_site.url
}